import gleam/dynamic
import gleam/json
import gleeunit
import gleeunit/should
import plinth/javascript/storage
import varasto

pub fn main() {
  gleeunit.main()
}

pub fn get_set_test() {
  use <- run()
  let assert Ok(local) = storage.local()
  let s = varasto.new(local, int_list_reader(), int_list_writer())
  should.be_ok(varasto.set(s, "Foo", [1, 2, 3]))
  should.equal(varasto.get(s, "Foo"), Ok([1, 2, 3]))
}

pub fn set_limit_test() {
  use <- run()
  let assert Ok(local) = storage.local()
  let s = varasto.new(local, str_reader(), str_writer())
  should.be_ok(varasto.set(s, "Foo1", "Bar"))
  should.be_ok(varasto.set(s, "Foo2", "Bar"))
  should.be_ok(varasto.set(s, "Foo3", "Bar"))
  should.be_ok(varasto.set(s, "Foo4", "Bar"))
  should.be_ok(varasto.set(s, "Foo5", "Bar"))
  should.be_error(varasto.set(s, "Foo6", "Bar"))
}

pub fn remove_test() {
  use <- run()
  let assert Ok(local) = storage.local()
  let s = varasto.new(local, str_reader(), str_writer())
  should.equal(varasto.remove(s, "not here"), Nil)
  should.be_ok(varasto.set(s, "Foo", "Bar"))
  should.equal(varasto.remove(s, "Foo"), Nil)
  should.be_error(varasto.get(s, "Foo"))
}

pub fn clear_test() {
  use <- run()
  let assert Ok(session) = storage.session()
  let s = varasto.new(session, str_reader(), str_writer())
  should.be_ok(varasto.set(s, "Foo1", "Bar"))
  should.be_ok(varasto.set(s, "Foo2", "Bar"))
  should.be_ok(varasto.set(s, "Foo3", "Bar"))
  should.be_ok(varasto.set(s, "Foo4", "Bar"))
  should.be_ok(varasto.set(s, "Foo5", "Bar"))
  varasto.clear(s)
  should.equal(storage.length(session), 0)
}

fn int_list_reader() {
  dynamic.list(dynamic.int)
}

fn int_list_writer() {
  fn(val: List(Int)) { json.array(val, json.int) }
}

fn str_reader() {
  dynamic.string
}

fn str_writer() {
  fn(val: String) { json.string(val) }
}

@external(javascript, "./storage_test_ffi.mjs", "runWithMockStorage")
fn run(callback: fn() -> a) -> Nil
